import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import "./App.css";
import React from "react";

import Logo from "../componentes/templates/logo/Logo";
import Nav from "../componentes/templates/nav/Nav";
import Main from "../componentes/templates/main/Main";
import Footer from "../componentes/templates/footer/Footer";

export default props => (
  <div className="app">
    <Logo />
    <Nav />
    <Main />
    <Footer />
  </div>
);
